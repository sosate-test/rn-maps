import { createLogger } from 'redux-logger';
import { createStore, applyMiddleware } from 'redux';


// import InitialState from './initialState';
import Reducers from '@redux/reducers';


const loggerMiddleware = createLogger();
const rootStore = createStore(
    Reducers,
    
    applyMiddleware(
        loggerMiddleware
    ));

export default rootStore;
