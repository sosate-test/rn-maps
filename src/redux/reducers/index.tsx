import { combineReducers } from 'redux';
import Candidates from './Candidates';



/** Nota ojo tener en cuenta tener nombres diferentes para no solo
para el nombhre de la funcion  si no para el type de una accion ya
que si muchas acciones  se ejecutaran  para todas las acciones con el mismo nombre
*/


//combineReducers nos permite crear diferentes combineReducers
//aquí le pasamos el reducer que llama al archivo json con los superheroes
const roorReducer = combineReducers({
    candidates: Candidates,
});

export default roorReducer;
