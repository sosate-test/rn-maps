import { RootStateOrAny } from "react-redux";


export function generarNumero(numero:number) {
    return (Math.random() * numero).toFixed(0);
}

/**
 *  Funcion para generar colores randon RGB.
 * @param numero
 */
export function colorRGB() {
    var coolor = "(" + generarNumero(255) + "," + generarNumero(255) + "," + generarNumero(255) + ")";
    return "rgb" + coolor;
}


/** Funcion para obtener lel tiempo que ha pasado  */
export function _obtenerDias(fechaInput : any) {

    try {
        let fechaI = new Date(fechaInput);

        var fechaActual = "";
        const fechaF = new Date();

        var diferencia = fechaI.getTime() - fechaF.getTime();

        var hora: any = fechaI.getHours();
        var minutos: any = fechaI.getMinutes();

        var ampm = hora >= 12 ? 'pm' : 'am';//determinar que horario essta
        var hora_ampm = hora > 12 ? hora - 12 : hora == 0 ? 12 : hora; //determinar la hora

        var segsMilli = 1000;
        var minsMilli = segsMilli * 60;
        var horasMilli = minsMilli * 60;
        var diasMilli = horasMilli * 24;

        var diasTranscurridos  = (diferencia / diasMilli);
        if (diasTranscurridos < 0) {
            diasTranscurridos *= -1;
        }

        diasTranscurridos = parseInt(diasTranscurridos+"");
        // console.log(diasTranscurridos);


        if (diasTranscurridos == 0) {
            fechaActual = "Hoy a las " + hora_ampm + ':' + minutos + " " + ampm;
        } else if (diasTranscurridos == 1) {
            fechaActual = "Ayer a las " + hora_ampm + ':' + minutos + " " + ampm;
        } else {
            fechaActual = "Hace " + diasTranscurridos + " días a las " + hora_ampm + ':' + minutos + " " + ampm;
        }

        return fechaActual;



    } catch (e) {
        console.log(e.message);
    }

}