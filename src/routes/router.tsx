import React from 'react';
import { View, Text } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';


// Rutas
import * as RUTAS from './rutas'
const Stack = createStackNavigator();


interface Props { }
function start(props: Props) {
    return (
        <Stack.Navigator headerMode={"none"}>
            <Stack.Screen name="Maps" component={RUTAS.Maps} />
            <Stack.Screen name="Lista" component={RUTAS.Lista} />
            <Stack.Screen name="Detalle" component={RUTAS.Detalle} />
        </Stack.Navigator>
    );
}

export default start;