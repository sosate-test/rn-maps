const example = [
    {
        candidates:[
            {
                name: "Brandon Henao",
                rating: 5
            },
            {
                name: "Gloria Henao",
                rating: 5
            }
        ],
        location:{
            lat: 3.38343663,
            long: -76.55699634
        },
        date: new Date()
    },
    {
        candidates: [
            {
                name: "Angulo Preciado",
                rating: 5
            }
        ],
        location: {
            lat: 3.38543663,
            long: -76.55701634
        },
        date: new Date()
    }
    
]


export default example;