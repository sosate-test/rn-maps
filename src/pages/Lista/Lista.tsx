import React, { Component, createRef, RefObject } from 'react'
import {
    Alert,
    Modal,
    StyleSheet,
    Text,
    TouchableHighlight,
    TouchableOpacity,
    View,
    FlatList
} from 'react-native'

import { showMessage, hideMessage } from "react-native-flash-message";
import { Header } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Card, Divider } from 'react-native-elements'



/**Importar redux */
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import store from '@redux/store';// vemos el estado actual del store
import { accionCandidates } from '@redux/accions'

/** Utilidades */
import * as Utils from '@helper/utils';


export interface Props {
    addUbicacionesVisibleFunc: Function,
    modalVisible: boolean,
    candidates: any,
    navigation: any
}
interface State {
    candidatesLocated:any[]
}


class Lista extends Component<Props, State> {
    unsubscribe:any;

    constructor(props: Props) {
        super(props);

        this.state = {
            candidatesLocated:[]
        }

        // me subscirbo a redux , para saber cuando hay un cambio en el store y refrescar los cambios
        this.unsubscribe = store.subscribe(this.handleChange.bind(this));
       
    }

    componentWillUnmount() {
        this.unsubscribe();
    }

    componentDidMount(){
        this.handleChange();
    }

    handleChange() {
        let currentValue = store.getState().candidates.candidatesLocated;
        //console.log(currentValue);
        if (currentValue != null) {
            this.setState({ candidatesLocated: currentValue})
        }
    }


    render() {
        const { candidates } = this.props;


        return (
            <View style={styles.centeredView}>


                <Header
                    leftComponent={
                        <TouchableOpacity
                            hitSlop={{ top: 20, bottom: 20, left: 20, right: 20 }}
                            onPress={() => {
                                this.props.navigation.pop();
                            }}
                        >
                            <Icon
                                name="chevron-left"
                                size={16}
                                color="#fff"
                            />
                        </TouchableOpacity>

                    }
                    centerComponent={{ text: 'Lista Pucher', style: { color: '#fff', fontWeight: "600", fontSize: 18 } }}
                />

                <FlatList
                    data={this.state.candidatesLocated.reverse()}
                    renderItem={this._renderItem}
                    keyExtractor={({ item, index }: { item: any, index: number }) => index+""}
                    //extraData={candidates}
                />


            </View>
        )
    }

    _renderItem = ({ item, index }: { item: any, index: number }) => {
        return (
            <TouchableOpacity key={index} onPress={() => { this.props.navigation.navigate("Detalle", { data: { item, index } }); }}  style={[styles.renderContentList]}>
                <View style={[styles.viewRenderContentList]}>
                    <Text style={[styles.textStyleDate]}>{Utils._obtenerDias(item.date)}</Text>

                    <View style={[styles.viewBodyRender]}>

                        <View style={[styles.avatarStyle]}>
                            <Text style={[styles.textAvatarStyle]}>{item.candidates.length}</Text>
                        </View>


                        <View style={[styles.viewCandidatos]}>
                            <Text style={[styles.textStyleTituCandidatos]}>{"Candidatos"}</Text>
                            {
                                item.candidates.map((item: any,index:any) => {
                                    return (
                                        <Text key={index} style={[styles.textStyle]}>{item.name}</Text>
                                    )
                                })
                            }
                        </View>
                    </View>
                    {/* <Text style={[styles.textStyle]}>{item.date.toString("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")}</Text>  */}
                </View>
                <Divider />
            </TouchableOpacity>
        )
    }

    customAlert() {
        showMessage({
            message: "Alerta",
            description: "Mensaje de prueba",
            type: "warning",
        });
    }

}


const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        //justifyContent: "center",
        //alignItems: "center",
        //marginTop: 22,
        backgroundColor: 'white'
    },
    openButton: {
        backgroundColor: "#F194FF",
        borderRadius: 20,
        padding: 10,
        elevation: 2
    },
    textStyleDate: {
        textAlign: "right",
        fontSize: 16,
    },
    textStyleTituCandidatos: {
        textAlign: "left",
        fontSize: 18,
        fontWeight: "600",
        marginBottom: 2
    },
    textAvatarStyle: {
        fontSize: 20,
        textAlign: "center",
    },
    textStyle: {
        fontSize: 16,
    },
    avatarStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: "center",
        width: 50,
        height: 50,
        backgroundColor: 'rgba(0,0,0,0.2)',
        borderRadius: 100
    },
    renderContentList: {
        paddingTop: "5%"
    },
    viewRenderContentList: {
        paddingHorizontal: "5%"
    },
    viewCandidatos: {
        paddingHorizontal: "5%",
        flex: 1,
        justifyContent: "center"
    },
    viewBodyRender: {
        flexDirection: "row",
        marginBottom: 5
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    }
});




const mapStateToProps = (state: any) => {
    return {
        candidates: state.candidates
    }
}

//const mapDispatchToProps = { accionCandidates }
const mapDispatchToProps = (dispatch: any) => {
    return {
        actions: {
            AccionCandidates: bindActionCreators(accionCandidates, dispatch)
        }
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Lista);