import React, { Component } from 'react'
import {
    Text,
    View,
    StyleSheet,
    Alert
} from 'react-native'

import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps'; // remove PROVIDER_GOOGLE import if not using Google Maps
import { Container, Content, Footer, Button } from 'native-base';
import { showMessage, hideMessage } from "react-native-flash-message";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Geolocation from '@react-native-community/geolocation';
import Pusher from 'pusher-js/react-native';



/**Importar redux */
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import { accionCandidates } from '@redux/accions'


//Utilidades/constantes
import * as Utils from '@helper/utils';
import ejemplo from '@constans/ejemplo';
import pusherConfig from '@constans/pusher_conf';
import * as MapsInterfaces from './Mapas.interfaces';






class Maps extends Component<MapsInterfaces.Props, MapsInterfaces.State> {
    pusher: any;
    mapsComponent: any;
    chatChannel: any;

    constructor(props: MapsInterfaces.Props) {
        super(props);

        this.state = {
            initialRegion: { 
                latitude: 0, 
                longitude: 0, 
                latitudeDelta: 0.015,
                longitudeDelta: 0.0121,
            },
            miMarker: {
                latitude: 0,
                longitude: 0
            },
            isChange: true,
            addUbicacionesVisible: false,
            candidatesLocated: []
        };

   
        this.pusher = new Pusher(pusherConfig.key, pusherConfig); // (1) inicializar pusher con la configuracion desde pusher.js
        this.chatChannel = this.pusher.subscribe('candidates_channel'); // (2) nos suscribimos al canal Pusher al que nuestro servidor agrega todos los mensajes

        this.chatChannel.bind('pusher:subscription_succeeded', () => { // (3) Esta es una devolución de llamada cuando la suscripción ha sido exitosa, ya que es un evento asincrónico
            this.chatChannel.bind('addCandidates', (data: MapsInterfaces.Pusher) => { // (4) 
                this.handleAddCandidates(data);
            });
        });
    }


    async UNSAFE_componentWillMount() {
        await this.geolocation(false);
    }

    geolocation(isInit = true) {
        Geolocation.getCurrentPosition(
            position => {
                //console.log(position.coords)

                let initialRegion = {
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                    latitudeDelta: 0.015,
                    longitudeDelta: 0.0121,
                }

                this.setState({ initialRegion }, () => {

                    if (!isInit) {
                        this.mapsComponent.animateToRegion(initialRegion, 200);
                    }
                });


            },
            error => console.log('Error', JSON.stringify(error)),
            //{ enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
        );
    }

    /** cada vez que recibimos un joinmensaje en el canal y agrega un mensaje a nuestra lista. */
    handleAddCandidates(data: MapsInterfaces.Pusher) {

        const { candidates, actions } = this.props;

        candidates.candidatesLocated.push(data);
        this.setState({
            candidatesLocated: candidates.candidatesLocated
        }, () => actions.AccionCandidates.setCandidates(candidates));

        showMessage({
            message: "Se añadio un nuevo registro",
            description: "",
            type: "success",
        });

    }

    render() {

        return (
            <View style={styles.container}>
                <MapView
                    ref={(componet: any) => { this.mapsComponent = componet }}
                    provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                    style={styles.map}
                    initialRegion={this.state.initialRegion}
                    //onRegionChange={this.onRegionChange.bind(this)}
                >

                    {this.state.initialRegion &&
                        <Marker
                            key={0}
                        coordinate={this.state.initialRegion}
                            title={"Estas aqui"}
                        />
                    }


                    {
                        this.state.candidatesLocated.map((item: any, index: number) => {
                            return (
                                <Marker
                                    key={index + 1}
                                    coordinate={{
                                        latitude: item.location.lat,
                                        longitude: item.location.lng
                                    }}
                                    onPress={() => {
                                        this.props.navigation.navigate("Detalle", { data: { item, index } });
                                    }}
                                    // pinColor={Utils.colorRGB()}
                                    pinColor={"blue"}
                                />
                            )
                        })
                    }
                </MapView>

                {
                    this.state.isChange ?
                        <View style={{ width: '20%', position: 'absolute', right: 0, bottom: 160 }}>
                            <Button
                                style={styles.buttonIcon}
                                onPress={() => this.geolocation(false)}>
                                <Icon
                                    style={styles.Icon}
                                    name='crosshairs-gps'
                                    size={26}
                                    color='black' />
                            </Button>
                        </View> : null
                }


                <View style={{ width: '90%', position: 'absolute', bottom: 40 }}>
                    <Button
                        style={styles.button}
                        onPress={() => this.validateList()}>
                        <Text style={styles.buttonText} > {"Listar"} </Text>
                    </Button>
                </View>


            </View>
        )
    }


    /**
     * Funcion para cambiar el estado de visible del modal añadir ubicaciones.
     */
    modalVisible() {
        this.setState({ addUbicacionesVisible: !this.state.addUbicacionesVisible })
    }

    /**
     * Funcion para capturar y cambiar la region actual
     * @param {*} region region / ubicacion
     */
    onRegionChange(miMarker: any) {
        this.setState({ miMarker });
        if (!this.state.isChange) {
            this.setState({ isChange: true });
        }
    }


    /**
     * Funcion de alerta de ejemplo
     */
    validateList() {

        if (this.state.candidatesLocated.length > 0) {
            this.props.navigation.navigate("Lista")
        } else {
            showMessage({
                message: "Señor usuario en el momento no tenemos nada para mostrarte.",
                description: "",
                type: "info",
            });
        }

    }

}


const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        height: "100%",
        width: "100%",
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    button: {
        //flex: 0.2,
        //width: '100%'
        backgroundColor: 'rgba(54,111,210,0.8)',
        borderRadius: 8,

    },
    buttonIcon: {
        height: 55,
        width: 55,
        backgroundColor: 'rgba(255,255,255,1)',
        borderRadius: 100,
        elevation: 3,

        shadowOffset: { width: 6, height: 6 },
        shadowColor: 'rgba(0,0,0,0.4)',
        shadowOpacity: 1,
        // background color must be set
        //backgroundColor: "#0000" // invisible color

    },
    Icon: {
        flex: 1,
        textAlign: 'center',
        color: 'black',
        fontWeight: '600'
    },
    buttonText: {
        flex: 1,
        textAlign: 'center',
        fontSize: 18,
        color: 'white',
        fontWeight: '600'
    }
});




const mapStateToProps = (state: any) => {
    return {
        candidates: state.candidates
    }
}

//const mapDispatchToProps = { accionCandidates }
const mapDispatchToProps = (dispatch: any) => {
    return {
        actions: {
            AccionCandidates: bindActionCreators(accionCandidates, dispatch)
        }
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Maps);