


export interface Location {
    lat: number,
    long: number,
}

export interface Candidates {
    name: string,
    rating: number,
}


export interface Pusher {
    candidates: Candidates[],
    location: Location,
    date: Date
}


export interface Props {
    candidates: any,
    navigation: any,
    actions:any
}


export interface State {
    addUbicacionesVisible: boolean,
    isChange: boolean,
    initialRegion: any,
    candidatesLocated:any[],
    miMarker:any
}

