import React, { Component, createRef, RefObject } from 'react'
import {
    Alert,
    Modal,
    StyleSheet,
    Text,
    TouchableHighlight,
    TouchableOpacity,
    View,
    FlatList,
    ScrollView,
    ActivityIndicator
} from 'react-native'

import { showMessage, hideMessage } from "react-native-flash-message";
import { Header } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Card, Divider, Rating, AirbnbRating } from 'react-native-elements'
import MapView, { Marker } from 'react-native-maps';

/**Importar redux */
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import { accionCandidates } from '@redux/accions'

/** Utilidades */
import * as Utils from '@helper/utils';


export interface Props {
    route: any,
    navigation: any
}
interface State {
    candidatesSelected: any,
    region: any
}


class Detalle extends Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
            candidatesSelected: props.route.params.data.item,
            region: {
                latitude: props.route.params.data.item.location.lat,
                longitude: props.route.params.data.item.location.lng,
                latitudeDelta: 0.002,
                longitudeDelta: 0.002
            }
        }
    }


    render() {

        return (
            <View style={styles.centeredView}>


                <Header
                    leftComponent={
                        <TouchableOpacity
                            hitSlop={{ top: 20, bottom: 20, left: 20, right: 20 }}
                            onPress={() => {
                                this.props.navigation.pop();
                            }}
                        >
                            <Icon
                                name="chevron-left"
                                size={16}
                                color="#fff"
                            />

                        </TouchableOpacity>

                    }
                    centerComponent={{ text: 'Detalle', style: { color: '#fff', fontWeight: "600", fontSize: 18 } }}
                />


                    {
                        (this.state.candidatesSelected) ?
                            <MapView
                                provider={"google"}
                                style={[{ height: 200, backgroundColor: "rgba(0,0,0,0.05)" }]}
                                scrollEnabled={false}
                                zoomEnabled={false}
                                pitchEnabled={false}
                                rotateEnabled={false}
                                cacheEnabled={true} // for ios , If true map will be cached and displayed as an image 
                                initialRegion={this.state.region}
                            >
                                <Marker
                                    coordinate={this.state.region}
                                    pinColor={"blue"}
                                />
                            </MapView>
                            :
                            <View style={{ zIndex: 0, flex: 1, justifyContent: 'center', position: 'absolute', right: 0, left: 0, top: 0, bottom: 0, backgroundColor: 'rgba(200, 200, 200, 0.0)' }}>
                                <ActivityIndicator size={30} color="rgba(94,138,201,0.5)" />
                            </View>
                    }

                    <View style={styles.viewBodyRenderHeader}>
                        <Text style={[styles.textStyleTituCandidatos]}>{"Candidatos"}</Text>
                        <Text style={[styles.textStyleDate]}>{Utils._obtenerDias(this.state.candidatesSelected.date)}</Text>
                   </View>
                    
                    <FlatList
                        data={this.state.candidatesSelected.candidates}
                        renderItem={this._renderItem}
                        keyExtractor={({ item, index }: { item: any, index: number }) => item}
                    />

            </View>
        )
    }

    _renderItem = ({ item, index }: { item: any, index: number }) => {
       
        return (
            <View key={index} style={[styles.renderContentList]}>
                <View style={[styles.viewRenderContentList]}>
                    
                    <View style={[styles.viewBodyRender]}>

                        <View style={[styles.avatarStyle]}>
                            <Text style={[styles.textAvatarStyle]}>{item.name[0]}</Text>
                        </View>

                        <View style={[styles.viewCandidatos]}>
                            <Text style={[styles.textStyle]}>{item.name}</Text>
                            <Rating
                                key={index}
                                readonly
                                ratingCount={5}
                                startingValue={item.rating}
                                style={styles.ratingStyle}
                            />
                        </View>
                    </View>


                   
    
                </View>
                <Divider />
            </View>
        )
    }

    customAlert() {
        showMessage({
            message: "Alerta",
            description: "Mensaje de prueba",
            type: "warning",
        });
    }

}


const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        //justifyContent: "center",
        //alignItems: "center",
        //marginTop: 22,
        backgroundColor: 'white'
    },
    openButton: {
        backgroundColor: "#F194FF",
        borderRadius: 20,
        padding: 10,
        elevation: 2
    },
    ratingStyle:{
        backgroundColor: "red", 
        alignSelf:"flex-start" 
    },
    textStyleDate: {
        textAlign: "right",
        fontSize: 16,
    },
    textStyleTituCandidatos: {
        textAlign: "left",
        fontSize: 24,
        fontWeight: "600",
       
    },
    textAvatarStyle: {
        fontSize: 20,
        textAlign: "center",
    },
    textStyle: {
        fontSize: 20,
    },
    avatarStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: "center",
        width: 50,
        height: 50,
        backgroundColor: 'rgba(0,0,0,0.2)',
        borderRadius: 100
    },
    renderContentList: {
        paddingTop: "5%"
    },
    viewRenderContentList: {
        paddingHorizontal: "5%"
    },
    viewCandidatos: {
        paddingHorizontal: "5%",
        flex: 1,
        justifyContent: "center"
    },
    viewBodyRender: {
        flexDirection: "row",
        marginBottom: 5
    },
    viewBodyRenderHeader: {
        flexDirection: "row",
        marginHorizontal:'2%',
        marginBottom: 5,
        justifyContent:"space-between"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    }
});




const mapStateToProps = (state: any) => {
    return {
        candidates: state.candidates
    }
}

//const mapDispatchToProps = { accionCandidates }
const mapDispatchToProps = (dispatch: any) => {
    return {
        actions: {
            AccionCandidates: bindActionCreators(accionCandidates, dispatch)
        }
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Detalle);