/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */


import 'react-native-gesture-handler';
import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import FlashMessage from "react-native-flash-message";


import { Provider } from 'react-redux';
import ROUTER from "@routes/router";
import store from '@redux/store';



class App extends Component {
  render() {
    return (
      <NavigationContainer>
        
        <Provider store={store}>
          <ROUTER />
        </Provider>
        
        <FlashMessage
          position="bottom"
          duration={3000}
          titleStyle={[{ fontSize: 18, color: "white" }]}
          textStyle={[{ fontSize: 16, color: "white", fontWeight: "600" }]}
        />
      </NavigationContainer>
    )
  }
}


export default App;
