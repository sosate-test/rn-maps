> # Brandon Henao


## Despliege
El despliegue se hace a traves de [Diawi](https://www.diawi.com/), esta es la url base donde podemos descargar el [apk](https://i.diawi.com/kxmCbL) para android , **este link esta habilitado solo para dos dias, a partir de 01/sep/2020**.

> **Note:** el servicio esta alojado en el servidor con una cuenta gratuita , por ende puede tener retrasos o incluso perdida de la informacion


### Demo
En la imagen se demuestra como podemos empezar a usar nuestra aplicación.  

![demo](readme-assets/demo.gif)


## Herramientas
- Visual studio code
- Genymotion
- Xcode
- LICEcap


## Tecnologías
- React Native 0.63
- React Navigation
- Pusher
- native-base
- react-native-vector-icons
- react-native-elements
- rutas absolutas  
- redux

  npm install --save-dev babel-plugin-module-resolver
```sh
    {
    "plugins": [
        ["module-resolver", {
        "root": ["./src"],
        "alias": {
            "test": "./test",
            "underscore": "lodash"
        }
        }]
    ]
    }
```

- react-native-maps
[Google Console](https://console.cloud.google.com/)
- @react-native-community/geolocation , se puede usar este react-native-geolocation-service , para mejorar el servicio


## Comandos

    npx pod-install ios instalar dependencias en IOS.
    npx react-native run-ios --simulator="iPhone 11" -> comando para instalar una version de desarrollo en un emilador IOS.
    npx react-native run-android --variant=release -> comando para intalar una version de produccion en el dispositivo Android.