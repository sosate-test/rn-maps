module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [["module-resolver",
    {
      "alias": {
        "@src": "./src",
        "@routes": "./src/routes",
        "@pages": "./src/pages",
        "@constans": "./src/constans",
        "@assets": "./src/assets",
        "@helper": "./src/helper",
        "@api": "./src/api",
        "@redux": "./src/redux",
      }
    }]]
};
